## Welcome

Thanks for considering contributing to Rabbit for Reddit.  
The goal with this project is to create and publish a completely open sourced client for Reddit built in Flutter. You can help by contributing in many different ways - submitting bug reports, requesting features, suggest ways to improve code quality, writing tests, or writing code for the app. All kinds of contributions is more than welcome!

Contributors can communicate using the GitLab issue tracker or our [Discord](https://discord.gg/sm8PA6s).

## Getting started

To contribute to the development of this project:

1. Create a fork of this project.
2. Clone your fork locally.
3. Follow the **Getting started** section in the [README](/README.md).
4. Do your changes in the fork.
5. Create a merge request. For info on how to make a merge request, see [New merge request from a fork](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-a-fork) by GitLab.

If you're just proposing a small or obvious fix, such as a clean up, typo correction or similar, it would be fine to just make a merge request without any discussion beforehand.  
For larger contributions such as new features, bug fixes, etc. there has to be an open issue in the issue tracker so specific users can be assigned as responsible for moving the issue forward. 

## New features

Some new features may require changes in an additional project - [rednit](https://gitlab.com/alfonsinbox/rednit). Rednit is the Reddit API Wrapper used in this app and currently does not cover the entire Reddit API. It is also open source, so you may contribute to rednit as well.
