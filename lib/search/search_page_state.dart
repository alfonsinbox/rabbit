import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rednit/rednit.dart';

part 'search_page_state.freezed.dart';

@freezed
abstract class SearchPageState with _$SearchPageState {
  const factory SearchPageState.initial() = _SearchPageStateInitial;
  const factory SearchPageState.loading() = _SearchPageStateLoading;
  const factory SearchPageState.debouncing() = _SearchPageStateDebouncing;
  const factory SearchPageState.success({
    required Listing<Subreddit> searchResults,
  }) = _SearchPageStateSuccess;
  const factory SearchPageState.empty() = _SearchPageStateEmpty;
}
