import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'search_page_model.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late SearchPageModel _model;

  @override
  void initState() {
    super.initState();
    _model = SearchPageModel(
      redditClient: Provider.of(context, listen: false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SearchPageModel>(
      model: _model,
      builder: (context, model, child) {
        return Scaffold(
          appBar: AppBar(
            titleSpacing: 0.0,
            title: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: TextField(
                maxLines: 1,
                onChanged: model.search,
                decoration: InputDecoration(
                  isDense: true,
                  fillColor: Colors.black12,
                  contentPadding: EdgeInsets.all(12.0),
                  hintText: 'Search',
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: Colors.transparent,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                    borderSide: BorderSide(
                      color: Colors.transparent,
                    ),
                  ),
                ),
              ),
            ),
          ),
          body: SafeArea(
            child: CustomScrollView(
              slivers: <Widget>[
                // SliverList(
                //   delegate: SliverChildListDelegate.fixed([]),
                // ),
                model.state.when(
                  initial: () => SliverFillRemaining(
                    child: Center(child: Text('Search for whatever!')),
                  ),
                  loading: () => SliverFillRemaining(
                    child: Center(child: CircularProgressIndicator()),
                  ),
                  debouncing: () => SliverFillRemaining(
                    child: Center(child: Text('Debouncing...')),
                  ),
                  success: (searchResults) => SliverList(
                    delegate: SliverChildListDelegate([
                      for (final result in searchResults.children)
                        ListTile(
                          title: Text('${result.displayName}'),
                        ),
                    ]),
                  ),
                  empty: () => SliverFillRemaining(
                    child: Center(
                      child: Text('Could not find anything!'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
