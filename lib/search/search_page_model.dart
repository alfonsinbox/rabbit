import 'dart:async';

import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';
import 'package:rxdart/rxdart.dart';

import 'search_page_state.dart';

class SearchPageModel extends BaseModel {
  final RedditClient _redditClient;

  final _inputSearchEvents = StreamController<String>();

  SearchPageState _state = SearchPageState.initial();
  SearchPageState get state => _state;
  set state(SearchPageState value) {
    _state = value;
    print('Setting state $value');
    notifyListeners();
  }

  late StreamSubscription<SearchPageState> _searchListener;

  SearchPageModel({
    required RedditClient redditClient,
  }) : _redditClient = redditClient {
    // TODO: Handle empty values, set state to initial

    _searchListener = _inputSearchEvents.stream
        .debounceTime(const Duration(milliseconds: 500))
        .switchMap(_performSearch)
        .listen((state) => this.state = state);
  }

  @override
  void dispose() {
    _searchListener.cancel();
    super.dispose();
  }

  void search(String query) => _inputSearchEvents.add(query);

  Stream<SearchPageState> _performSearch(String query) async* {
    if (query.isEmpty) {
      yield SearchPageState.initial();
    } else {
      yield SearchPageState.loading();
      final searchResults = await _redditClient.searchSubreddits(query);
      yield _resultsToState(searchResults);
    }
  }

  SearchPageState _resultsToState(Listing<Subreddit> searchResults) {
    if (searchResults.children.isEmpty) {
      return SearchPageState.empty();
    }
    return SearchPageState.success(searchResults: searchResults);
  }
}
