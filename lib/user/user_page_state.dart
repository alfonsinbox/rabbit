import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rednit/rednit.dart';

part 'user_page_state.freezed.dart';

@freezed
abstract class UserPageState with _$UserPageState {
  const factory UserPageState.loading({required User user}) = _Loading;
  const factory UserPageState.success({
    required User user,
    required List<Comment> comments,
    required List<Link> posts,
  }) = _Success;
}
