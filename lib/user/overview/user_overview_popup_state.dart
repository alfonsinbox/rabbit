import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rednit/rednit.dart';

part 'user_overview_popup_state.freezed.dart';

@freezed
abstract class UserOverviewPopupState with _$UserOverviewPopupState {
  const factory UserOverviewPopupState.loading({
    required String username,
  }) = _Loading;

  const factory UserOverviewPopupState.success({
    required User user,
  }) = _Success;

  const factory UserOverviewPopupState.error({
    required dynamic error,
  }) = _Error;
}
