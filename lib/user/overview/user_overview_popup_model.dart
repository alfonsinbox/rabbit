import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'user_overview_popup_state.dart';

class UserOverviewPopupModel extends BaseModel {
  final RedditClient _redditClient;

  UserOverviewPopupState _state;
  UserOverviewPopupState get state => _state;

  UserOverviewPopupModel({
    required RedditClient redditClient,
    required String username,
  })   : _redditClient = redditClient,
        _state = UserOverviewPopupState.loading(username: username) {
    _init();
  }

  Future<void> _init() async {
    final username = await _state.maybeWhen(
      loading: (username) async {
        print('Loading user $username');
        final user = await _redditClient.getUser(username);

        print('Loaded user $user');

        _state = UserOverviewPopupState.success(user: user);
        notifyListeners();
        return username;
      },
      orElse: () => null,
    );
  }
}
