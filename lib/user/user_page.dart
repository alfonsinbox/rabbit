import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rabbit/widgets/comment_widget.dart';
import 'package:rabbit/widgets/link_card.dart';
import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'user_page_model.dart';

class UserPage extends StatefulWidget {
  final User user;

  const UserPage({Key? key, required this.user}) : super(key: key);

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  late UserPageModel _model;

  @override
  void initState() {
    super.initState();
    _model = UserPageModel(
      redditClient: Provider.of(context, listen: false),
      user: widget.user,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<UserPageModel>(
      model: _model,
      builder: (context, model, child) {
        return DefaultTabController(
          length: 3,
          child: Scaffold(
            appBar: AppBar(
              title: Text(model.state.user.name),
              elevation: 0,
              bottom: TabBar(
                tabs: <Widget>[
                  Tab(text: 'Posts'.toUpperCase()),
                  Tab(text: 'Comments'.toUpperCase()),
                  Tab(text: 'About'.toUpperCase()),
                ],
              ),
            ),
            body: model.state.when(
              loading: (user) => Center(child: Text('...')),
              success: (user, comments, posts) {
                return TabBarView(
                  children: <Widget>[
                    ListView(
                      children: <Widget>[
                        for (final post in posts) LinkCard(link: post),
                      ],
                    ),
                    ListView(
                      children: <Widget>[
                        for (final comment in comments)
                          CommentWidget(comment: comment),
                      ],
                    ),
                    Column(children: <Widget>[]),
                  ],
                );
              },
            ),
          ),
        );
      },
    );
  }
}
