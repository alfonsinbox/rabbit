import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';

class FastScrollPhysics extends ScrollPhysics {
  @override
  FastScrollPhysics applyTo(ScrollPhysics? ancestor) {
    return FastScrollPhysics(parent: buildParent(ancestor));
  }

  FastScrollPhysics({ScrollPhysics? parent}) : super(parent: parent);

  @override
  SpringDescription get spring => SpringDescription.withDampingRatio(
      mass: 0.45, stiffness: 400, ratio: 1.0);
}
