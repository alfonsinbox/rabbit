import 'dart:async';

import 'package:simple_model_state/simple_model_state.dart';
import 'package:rednit/rednit.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../links.dart';
import 'profile_page_state.dart';

// enum ProfilePageState { initial, signedOut, signingIn, fetchingUser, profile }

class ProfilePageModel extends BaseModel {
  final RedditClient _redditClient;

  final List<StreamSubscription> _streamListeners = [];

  ProfilePageState _state = ProfilePageState.loading();
  ProfilePageState get state => _state;

  ProfilePageModel({
    required RedditClient redditClient,
    required LinksHandler linksHandler,
  }) : _redditClient = redditClient {
    _refreshState();
    _streamListeners.addAll([
      linksHandler.linkEvents.listen(_handleLinkEvent),
    ]);
  }

  @override
  void dispose() {
    _streamListeners.forEach((subscription) => subscription.cancel());
    super.dispose();
  }

  Future<void> _refreshState() async {
    final isSignedIn = await _redditClient.authManager.isSignedIn;
    if (isSignedIn) {
      _state = ProfilePageState.signedIn();
      notifyListeners();
      final me = await _redditClient.getMe();
      _state = ProfilePageState.profile(user: me);
      notifyListeners();
    } else {
      _state = ProfilePageState.signedOut();
      notifyListeners();
    }
  }

  Future<void> login() async {
    await state.maybeWhen(
      signedOut: () async {
        _state = ProfilePageState.signingIn();
        notifyListeners();

        final loginUrl =
            _redditClient.authManager.getLoginUrl(['identity', 'read', 'vote']);
        await launch(loginUrl, forceSafariVC: false);
        await _refreshState();
      },
      orElse: () async => null,
    );
  }

  Future<void> logout() async {
    await _redditClient.authManager.logout();
    await _refreshState();
  }

  Future<void> _handleLinkEvent(AppLinkEvent event) async {
    if (event is LoginRedirectLinkEvent) {
      await _redditClient.authManager.completeAuthorizationCodeFlow(
        state: event.state,
        code: event.code,
      );
      await _refreshState();
    }
  }
}
