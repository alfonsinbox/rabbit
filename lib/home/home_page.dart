import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'feed/feed_page.dart';
import 'profile/profile_page.dart';
import '../search/search_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  final List<_NavigationBarItem> navigationBarItems = [
    _NavigationBarItem(FeedPage(), FontAwesomeIcons.home, 'Home'),
    _NavigationBarItem(SearchPage(), FontAwesomeIcons.search, 'Search'),
    _NavigationBarItem(ProfilePage(), FontAwesomeIcons.user, 'Profile'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      // TODO: When possible would be nice to use FadeThrough switching here
      body: IndexedStack(
        index: _selectedIndex,
        children: navigationBarItems.map((item) => item.body).toList(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        items: [
          for (final item in navigationBarItems)
            BottomNavigationBarItem(
              icon: Center(
                child: FaIcon(item.iconData),
              ),
              label: item.iconTitle,
            ),
        ],
        onTap: (i) => setState(() => _selectedIndex = i),
        elevation: 0,
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: false,
        showSelectedLabels: false,
        selectedIconTheme: IconThemeData(
          opacity: 1,
          color: Theme.of(context).iconTheme.color,
        ),
        unselectedIconTheme: IconThemeData(
          opacity: 0.54,
          color: Theme.of(context).iconTheme.color,
        ),
      ),
    );
  }
}

class _NavigationBarItem {
  final Widget body;
  final IconData iconData;
  final String iconTitle;

  _NavigationBarItem(this.body, this.iconData, this.iconTitle);
}
