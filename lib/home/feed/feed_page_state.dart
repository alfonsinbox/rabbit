import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rednit/rednit.dart';

part 'feed_page_state.freezed.dart';

@freezed
abstract class FeedPageState with _$FeedPageState {
  const factory FeedPageState.loading() = FeedPageStateLoading;

  const factory FeedPageState.success({
    required List<Link> links,
  }) = FeedPageStateSuccess;

  const factory FeedPageState.gettingMore({
    required List<Link> links,
  }) = FeedPageStateGettingMore;
}
