import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rabbit/fast_scroll_physics.dart';
import 'package:simple_model_state/simple_model_state.dart';
import 'package:rednit/rednit.dart';

import 'detailed_link_page_model.dart';
import 'single/single_link_details_page.dart';

class DetailedLinkPage extends StatefulWidget {
  final Link link;
  final List<Link> allLinks;

  const DetailedLinkPage({
    Key? key,
    required this.link,
    required this.allLinks,
  }) : super(key: key);

  @override
  _DetailedLinkPageState createState() => _DetailedLinkPageState();
}

class _DetailedLinkPageState extends State<DetailedLinkPage> {
  late DetailedLinkPageModel _model;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _model = DetailedLinkPageModel(
      link: widget.link,
      redditClient: Provider.of(context, listen: false),
    );
    _pageController = PageController(
      initialPage: max(widget.allLinks.indexOf(widget.link), 0),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailedLinkPageModel>(
      model: _model,
      builder: (context, model, child) {
        return PageView.builder(
          controller: _pageController,
          physics: FastScrollPhysics(),
          itemCount: widget.allLinks.length,
          itemBuilder: (context, index) {
            return SingleLinkDetailsPage(
              link: widget.allLinks[index],
            );
          },
        );
      },
    );
  }
}
