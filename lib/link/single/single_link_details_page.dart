import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rednit/rednit.dart';
import 'package:shimmer/shimmer.dart';
import 'package:simple_model_state/simple_model_state.dart';
import 'package:better_color/better_color.dart';

import 'single_link_details_page_model.dart';
import '../../widgets/link_card.dart';
import '../../utilities/range_list.dart';
import '../../utilities/list_sum.dart';
import '../../widgets/comment_widget.dart';

class SingleLinkDetailsPage extends StatefulWidget {
  final Link link;

  const SingleLinkDetailsPage({Key? key, required this.link}) : super(key: key);

  @override
  _SingleLinkDetailsPageState createState() => _SingleLinkDetailsPageState();
}

class _SingleLinkDetailsPageState extends State<SingleLinkDetailsPage> {
  late SingleLinkDetailsPageModel _model;

  @override
  void initState() {
    super.initState();
    print('Some init');
    _model = SingleLinkDetailsPageModel(
      link: widget.link,
      redditClient: Provider.of(context, listen: false),
    );
  }

  @override
  Widget build(BuildContext context) {
    final hueValue = widget.link.subreddit.codeUnits.sum() % 360.0;
    return BaseWidget<SingleLinkDetailsPageModel>(
      model: _model,
      builder: (context, model, child) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Theme.of(context)
                .canvasColor
                .withSaturation(0.63)
                .withHue(hueValue),
          ),
          body: CustomScrollView(
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate.fixed([
                  LinkCard(
                    link: model.link,
                    expanded: true,
                    onTap: null,
                  ),
                ]),
              ),
              model.state.when(
                loading: (link) {
                  return SliverList(
                    delegate: SliverChildListDelegate.fixed([
                      Shimmer.fromColors(
                        baseColor:
                            Theme.of(context).brightness == Brightness.dark
                                ? Colors.grey.shade900
                                : Colors.grey.shade400,
                        highlightColor:
                            Theme.of(context).brightness == Brightness.dark
                                ? Colors.grey.shade600
                                : Colors.grey.shade200,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            for (final _ in 1.to(4))
                              Row(
                                children: <Widget>[
                                  _LoadingCommentWidget(),
                                ],
                              ),
                          ],
                        ),
                      ),
                    ]),
                  );
                },
                success: (link, comments) {
                  return SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return Column(
                          children: <Widget>[
                            CommentWidget(
                              comment: comments[index],
                              // colorCoded: true,
                            ),
                            Container(height: 8.0),
                          ],
                        );
                      },
                      childCount: comments.length,
                    ),
                  );
                },
              ),
            ],
          ),
        );
      },
    );
  }
}

class _LoadingCommentWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: Container(
              margin: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                color: Colors.white,
              ),
              height: 16.0,
              width: 96.0,
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: Container(
              margin: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                color: Colors.white,
              ),
              height: 12.0,
              width: 256.0,
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: Container(
              margin: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                color: Colors.white,
              ),
              height: 12.0,
              width: 256.0,
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(4.0),
            child: Container(
              margin: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                color: Colors.white,
              ),
              height: 12.0,
              width: 192.0,
            ),
          ),
        ],
      ),
    );
  }
}
