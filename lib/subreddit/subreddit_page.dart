import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'subreddit_page_model.dart';
import '../widgets/link_card.dart';
import '../widgets/subreddit_icon.dart';
import '../link/detailed_link_page.dart';

class SubredditPage extends StatefulWidget {
  final String subredditDisplayName;

  const SubredditPage({Key? key, required this.subredditDisplayName})
      : super(key: key);

  @override
  _SubredditPageState createState() => _SubredditPageState();
}

class _SubredditPageState extends State<SubredditPage> {
  late SubredditPageModel _model;

  @override
  void initState() {
    super.initState();
    _model = SubredditPageModel(
      subredditDisplayName: widget.subredditDisplayName,
      redditClient: Provider.of(context, listen: false),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SubredditPageModel>(
      model: _model,
      builder: (context, model, child) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.black,
          ),
          body: model.state.when(
            loading: () => Center(
              child: Text('Loading the sub'),
            ),
            success: (subreddit, links) => SafeArea(
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverList(
                    delegate: SliverChildListDelegate([
                      IntrinsicHeight(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Container(
                                child: SubredditIcon(
                                  subredditName: subreddit.displayName,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 8.0,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0,
                                      ),
                                      child: Text(
                                        subreddit.title,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0,
                                      ),
                                      child: Text(
                                        'r/${subreddit.displayName}',
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16.0,
                          vertical: 12.0,
                        ),
                        child: Text(subreddit.publicDescription),
                      ),
                      Divider(),
                    ]),
                  ),
                  SliverList(
                    delegate: SliverChildListDelegate([
                      for (final link in links)
                        LinkCard(
                          link: link,
                          onTap: () => Navigator.of(context).push(
                            MaterialPageRoute<void>(
                              builder: (context) => DetailedLinkPage(
                                link: link,
                                allLinks: links,
                              ),
                            ),
                          ),
                        ),
                    ]),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
