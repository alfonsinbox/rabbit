import 'package:rednit/rednit.dart';
import 'package:simple_model_state/simple_model_state.dart';

import 'subreddit_page_state.dart';

class SubredditPageModel extends BaseModel {
  final RedditClient _redditClient;
  final String subredditDisplayName;

  SubredditPageState _state = SubredditPageState.loading();
  SubredditPageState get state => _state;

  SubredditPageModel({
    required this.subredditDisplayName,
    required RedditClient redditClient,
  }) : _redditClient = redditClient {
    _init();
  }

  Future<void> _init() async {
    final subreddit = await _redditClient.getSubreddit(subredditDisplayName);
    final subredditLinks = await _redditClient.getSubredditFeed(
        subredditDisplayName, ListingType.best);

    _state = SubredditPageState.success(
      subreddit: subreddit,
      links: subredditLinks.children,
    );
    notifyListeners();
  }
}
