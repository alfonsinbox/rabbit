import 'dart:async';

import 'package:uni_links/uni_links.dart';

abstract class AppLinkEvent {}

class LoginRedirectLinkEvent extends AppLinkEvent {
  final String state, code;

  LoginRedirectLinkEvent({required this.state, required this.code});
}

class UnknownLinkEvent extends AppLinkEvent {
  final Uri uri;

  UnknownLinkEvent({required this.uri});
}

class LinksHandler {
  final _eventsController = StreamController<AppLinkEvent>.broadcast();
  Stream<AppLinkEvent> get linkEvents => _eventsController.stream;

  LinksHandler() {
    _eventsController.addStream(getUriLinksStream().map(_linkToEvent));
  }

  void dispose() {
    _eventsController.close();
  }

  AppLinkEvent _linkToEvent(Uri uri) {
    switch (uri.path) {
      case '/login-redirect':
        final state = uri.queryParameters['state'];
        if (state == null) throw StateError('State in auth request is null');

        final code = uri.queryParameters['code'];
        if (code == null) throw StateError('Code in auth request is null');

        return LoginRedirectLinkEvent(
          state: state,
          code: code,
        );
      default:
        return UnknownLinkEvent(uri: uri);
    }
  }
}
