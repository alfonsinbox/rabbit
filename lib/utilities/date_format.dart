extension ReadableDurationFormatter on Duration {
  String get readableShort {
    if (inDays >= 365) {
      return '${(inDays / 365).floor()}y';
    } else if (inDays >= 30) {
      return '${(inDays / 30).floor()}mo';
    } else if (inDays >= 1) {
      return '${(inDays)}d';
    } else if (inHours >= 1) {
      return '${(inHours)}h';
    } else if (inMinutes >= 1) {
      return '${(inMinutes)}m';
    } else {
      return '${(inSeconds)}s';
    }
  }
}
