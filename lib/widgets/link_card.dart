import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rabbit/user/overview/user_overview_popup.dart';
import 'package:rednit/rednit.dart';

import 'subreddit_icon.dart';
import 'vote_widget.dart';
import '../utilities/date_format.dart';
import '../subreddit/subreddit_page.dart';

class LinkCard extends StatelessWidget {
  final Link link;
  final void Function()? onTap;
  final bool expanded;

  LinkCard({
    Key? key,
    required this.link,
    this.onTap,
    this.expanded = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Material(
          // color: color,
          child: InkWell(
            onTap: onTap,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: IntrinsicHeight(
                      child: Row(
                        children: <Widget>[
                          SubredditIcon(
                            subredditName: link.subreddit,
                            onTap: () => Navigator.of(context).push(
                              MaterialPageRoute<void>(
                                builder: (context) => SubredditPage(
                                  subredditDisplayName: link.subreddit,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 8.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () => Navigator.of(context).push(
                                  MaterialPageRoute<void>(
                                    builder: (context) => SubredditPage(
                                      subredditDisplayName: link.subreddit,
                                    ),
                                  ),
                                ),
                                child: Text(
                                  'r/${link.subreddit}',
                                  overflow: TextOverflow.ellipsis,
                                  // style: TextStyle(fontSize: 18),
                                ),
                              ),
                              GestureDetector(
                                onTap: () => showDialog<void>(
                                  context: context,
                                  builder: (context) => UserOverviewPopup(
                                    username: link.author,
                                  ),
                                ),
                                child: Text(
                                  'Posted by u/${link.author}'
                                  ' • ${DateTime.now().toUtc().difference(link.createdUtc).readableShort}',
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        ?.color
                                        ?.withOpacity(0.54),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Text(
                      link.title,
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          ?.copyWith(fontSize: 18.0),
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: (() {
                      if (link.postHint == null) {
                        if (link.selftext == null || !expanded) {
                          return Container();
                        } else {
                          return Text(link.selftext);
                        }
                      }

                      switch (link.postHint) {
                        case 'image':
                          return _ImageTypeWidget(
                            thumbnailUrl: link.preview?.images?.first
                                    ?.resolutions.first.url ??
                                '',
                            imageUrl:
                                link.preview?.images?.first?.source.url ?? '',
                            expanded: expanded,
                          );
                        case 'link':
                          return _LinkTypeWidget(
                            linkUrl: link.url,
                            thumbnailUrl: link.thumbnail,
                          );
                        case 'self':
                          if (expanded) {
                            return Text(link.selftext);
                          }
                          return Container();
                        default:
                          return Center(
                            child: Text(
                              'Implement type ${link.postHint}',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  ?.copyWith(fontWeight: FontWeight.bold),
                            ),
                          );
                      }
                    })(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      VoteWidget(
                        score: link.score,
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              '${link.numComments}',
                              style: Theme.of(context).textTheme.button,
                            ),
                          ),
                          FaIcon(
                            FontAwesomeIcons.commentAlt,
                            size: 16.0,
                          ),
                          // Icon(Icons.mode_comment),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        // Divider(),
        Container(height: 8),
      ],
    );
  }
}

class _ImageTypeWidget extends StatelessWidget {
  const _ImageTypeWidget({
    Key? key,
    required this.thumbnailUrl,
    required this.imageUrl,
    required this.expanded,
  }) : super(key: key);

  final String imageUrl, thumbnailUrl;
  final bool expanded;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8.0),
      child: ConstrainedBox(
        constraints: expanded
            ? BoxConstraints.tightFor()
            : BoxConstraints(maxHeight: 200),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.network(
                // expanded ? imageUrl : thumbnailUrl,
                imageUrl,
                fit: BoxFit.cover,
                alignment: Alignment.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _LinkTypeWidget extends StatelessWidget {
  final String thumbnailUrl;
  final Uri linkUrl;

  const _LinkTypeWidget({
    Key? key,
    required this.thumbnailUrl,
    required this.linkUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8.0),
      child: IntrinsicHeight(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.black12,
              ),
            ),
            Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                Positioned.fill(
                  child: Image.network(
                    thumbnailUrl,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 48.0),
                  child: Container(
                    color: Colors.black87,
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      linkUrl.authority,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
