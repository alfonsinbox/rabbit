import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class VoteWidget extends StatelessWidget {
  final int score;
  final bool scoreHidden;

  const VoteWidget({
    Key? key,
    required this.score,
    this.scoreHidden = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        FaIcon(FontAwesomeIcons.arrowUp, size: 12.0),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            '${scoreHidden ? '?' : score}',
            style: Theme.of(context).textTheme.button,
          ),
        ),
        FaIcon(FontAwesomeIcons.arrowDown, size: 12.0),
      ],
    );
  }
}
