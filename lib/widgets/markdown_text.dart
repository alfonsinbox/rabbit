import 'package:flutter/material.dart';

class MarkdownText extends StatelessWidget {
  final String text;

  const MarkdownText({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(children: [
        for (final _ in [0])
          TextSpan(
            text: text,
            children: [],
          ),
      ]),
    );
  }
}
