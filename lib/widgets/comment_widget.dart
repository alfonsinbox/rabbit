import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rednit/rednit.dart';
import 'package:better_color/better_color.dart';

import '../utilities/date_format.dart';
import 'markdown_text.dart';
import 'vote_widget.dart';

class CommentWidget extends StatefulWidget {
  final Comment comment;
  final bool colorCoded;

  const CommentWidget({
    Key? key,
    required this.comment,
    this.colorCoded = false,
  }) : super(key: key);

  @override
  _CommentWidgetState createState() => _CommentWidgetState();
}

class _CommentWidgetState extends State<CommentWidget>
    with AutomaticKeepAliveClientMixin {
  bool collapsed = false;

  void toggleCollapsed() {
    if (!collapsed) HapticFeedback.lightImpact();
    setState(() => collapsed = !collapsed);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final backgroundColor = widget.colorCoded
        ? Theme.of(context)
            .canvasColor
            .withHue((widget.comment.depth * 45.0) % 360)
            .withSaturation(0.63)
        : Colors.transparent;

    return Container(
      color: backgroundColor,
      child: Column(
        children: <Widget>[
          IntrinsicHeight(
            child: Row(
              children: <Widget>[
                if (widget.comment.depth != null)
                  ...List.generate(widget.comment.depth, (i) {
                    final color = Theme.of(context).dividerColor;
                    final divider = VerticalDivider(
                      color: color.withOpacity(min(0.2 + (i) / 10, 1)),
                      width: 16.0, // i.e. padding + thickness
                    );
                    return divider;
                  }),
                if (widget.comment is MoreComment)
                  Expanded(
                    child: _MoreCommentsButton(
                      comment: widget.comment as MoreComment,
                    ),
                  ),
                if (widget.comment is TextComment) ...[
                  Expanded(
                    // TODO: Long press prevents scrolling in same gesture
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onLongPress: collapsed ? null : toggleCollapsed,
                      onTap: collapsed ? toggleCollapsed : null,
                      child: _TextCommentContent(
                        comment: widget.comment as TextComment,
                        collapsed: collapsed,
                      ),
                    ),
                  ),
                ],
              ],
            ),
          ),
          if (!collapsed)
            if (widget.comment is TextComment)
              if ((widget.comment as TextComment).replies != null)
                Column(
                  children: <Widget>[
                    for (final reply in (widget.comment as TextComment).replies)
                      CommentWidget(
                        comment: reply,
                        colorCoded: widget.colorCoded,
                      ),
                  ],
                ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _MoreCommentsButton extends StatelessWidget {
  final MoreComment comment;

  const _MoreCommentsButton({
    Key? key,
    required this.comment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        borderRadius: BorderRadius.circular(4.0),
        color: Colors.black12,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            '${comment.count} more replies',
            style: Theme.of(context).textTheme.button,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}

class _TextCommentContent extends StatelessWidget {
  final TextComment comment;
  final bool collapsed;

  const _TextCommentContent({
    Key? key,
    required this.comment,
    required this.collapsed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Opacity(
            opacity: 0.54,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    [
                      comment.author,
                      DateTime.now()
                          .difference(comment.createdUtc)
                          .readableShort,
                      if (collapsed) comment.body,
                    ].join(' • '),
                    maxLines: 1,
                    overflow: TextOverflow.clip,
                    softWrap: false,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        ?.copyWith(fontSize: 12.0),
                  ),
                ),
              ],
            ),
          ),
          if (!collapsed) ...[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: MarkdownText(text: comment.body),
            ),
            Padding(
              padding: const EdgeInsetsDirectional.only(end: 8.0, top: 8.0),
              child: Opacity(
                opacity: 0.54,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(),
                    ),
                    SizedBox(width: 24.0),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: FaIcon(FontAwesomeIcons.reply, size: 12.0),
                        ),
                        Text(
                          'Reply',
                          style: Theme.of(context).textTheme.button,
                        ),
                      ],
                    ),
                    SizedBox(width: 24.0),
                    VoteWidget(score: comment.score),
                  ],
                ),
              ),
            ),
          ],
        ],
      ),
    );
  }
}
